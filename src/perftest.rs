mod config;
mod handlers;
mod murmur;
mod murmur_rpc;
mod murmurgrpc;
mod murmurlistener;
mod state;
mod userinfo;

use crate::config::parse_config;
use murmurgrpc::MurmurGrpc;

#[tokio::main]
async fn main() {
    let args: Vec<String> = std::env::args().collect();
    let config_path = args.get(1).unwrap_or(&"config.json".to_owned()).clone();
    let config = match parse_config(&config_path) {
        Ok(config) => config,
        Err(_) => {
            println!("Failed to load config");
            return;
        }
    };

    let root_channel = config.root_channel.clone();

    let mut rpc = MurmurGrpc::new(&config.clone().grpc_url, &config.clone().auth_token);

    rpc.connect().await;

    let parent_channel = rpc.get_channel(&root_channel).await.unwrap();
    for i in 0..20 {
        rpc.create_channel(parent_channel.clone(), &i.to_string())
            .await;
    }
}
