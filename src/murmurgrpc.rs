use crate::murmur_rpc::{Channel, User};
use crate::{murmur, murmur_rpc};

#[derive(Clone, Debug)]
pub struct MurmurGrpc {
    client: Option<murmur_rpc::v1_client::V1Client<tonic::transport::Channel>>,
    grpc_url: String,
    auth_token: String,
}

impl MurmurGrpc {
    pub fn new(grpc_url: &str, auth_token: &str) -> Self {
        MurmurGrpc {
            client: None,
            grpc_url: grpc_url.to_owned(),
            auth_token: auth_token.to_owned(),
        }
    }

    pub async fn connect(&mut self) -> Result<bool, &'static str> {
        let grpc_url = self.grpc_url.clone();
        let auth_token = self.auth_token.clone();

        let client = murmur::create_sync_client(&grpc_url, &auth_token)
            .await
            .map_err(|_| "Failed connect sync client")?;

        self.client = Some(client);

        Ok(true)
    }

    fn user_connected(&mut self, nickname: &str) {
        println!("{} connected", nickname);
    }

    fn user_disconnected(&mut self, nickname: &str) {}

    pub async fn send_message_to_channel(&mut self, channel: murmur_rpc::Channel, message: &str) {
        let mut client = self.client.clone().unwrap();
        client
            .text_message_send(tonic::Request::new(murmur_rpc::TextMessage {
                server: Some(murmur_rpc::Server::new()),
                actor: None,
                users: vec![],
                channels: vec![channel],
                trees: vec![],
                text: Some(message.to_owned()),
            }))
            .await;
    }

    pub async fn broadcast_message(&mut self, message: &str) {
        let mut client = self.client.clone().unwrap();
        client
            .text_message_send(tonic::Request::new(murmur_rpc::TextMessage {
                server: Some(murmur_rpc::Server::new()),
                actor: None,
                users: vec![],
                channels: vec![],
                trees: vec![],
                text: Some(message.to_owned()),
            }))
            .await;
    }

    pub async fn create_channel(
        &mut self,
        parent_channel: murmur_rpc::Channel,
        channel_name: &str,
    ) -> Result<murmur_rpc::Channel, &str> {
        let mut client = self.client.clone().unwrap();

        let result = client
            .channel_add(tonic::Request::new(murmur_rpc::Channel {
                id: None,
                description: None,
                links: vec![],
                temporary: Some(true),
                server: Some(murmur_rpc::Server::new()),
                name: Some(channel_name.to_owned()),
                parent: Some(Box::new(parent_channel)),
                position: None,
            }))
            .await
            .map(|x| x.into_inner())
            .map_err(|e| "Failed to create channel");
        result
    }

    pub async fn get_channel(&mut self, channel_name: &str) -> Option<Channel> {
        let mut client = self.client.clone().unwrap();

        let channel_parts: Vec<&str> = channel_name
            .split("/")
            .filter(|x| !x.trim().is_empty())
            .collect();
        let result = client
            .channel_query(tonic::Request::new(murmur_rpc::channel::Query {
                server: Some(murmur_rpc::Server::new()),
            }))
            .await;

        let result = result.unwrap();
        let result = result.get_ref().channels.clone();

        let mut parent_channel: Option<murmur_rpc::Channel> = None;

        for (index, channel_part) in channel_parts
            .iter()
            .enumerate()
            .map(|(i, x)| (i, x.clone()))
        {
            let parent: Vec<murmur_rpc::Channel> = result
                .iter()
                .filter(|&c| {
                    let is_same_name = c.name == Some(channel_part.to_owned());
                    if index == 0 {
                        is_same_name
                    } else {
                        let current_parent_channel_id = c.parent.clone().map(|x| x.id).flatten();
                        let parent_channel_id = parent_channel.clone().map(|x| x.id).flatten();
                        let is_same_parent = current_parent_channel_id == parent_channel_id;
                        is_same_name && is_same_parent
                    }
                })
                .map(|x| x.clone())
                .collect();

            parent_channel = parent.first().map(|x| x.clone());
        }

        parent_channel
    }

    pub(crate) async fn get_channel_broken(
        &mut self,
        channel_name: &str,
    ) -> Option<murmur_rpc::Channel> {
        let mut client = self.client.clone().unwrap();

        let channel_parts: Vec<&str> = channel_name.split("/").collect();
        //dbg!(channel_parts);

        let result = client
            .channel_query(tonic::Request::new(murmur_rpc::channel::Query {
                server: Some(murmur_rpc::Server::new()),
            }))
            .await;

        let result = result.unwrap();
        let result = result.get_ref().channels.clone();

        let mut parent_id = None;

        let mut filtered: Vec<murmur_rpc::Channel> = vec![];
        for channel_part in channel_parts.iter().map(|x| x.clone()) {
            dbg!(channel_part);
            if parent_id == None {
                filtered = result
                    .iter()
                    .filter(|c| c.name == Some(channel_part.to_owned()))
                    .map(|x| x.clone())
                    .collect();
                parent_id = filtered.get(0).map(|x| x.id)
            } else {
                filtered = result
                    .iter()
                    .filter(|c| {
                        return if let Some(c_parent) = c.parent.clone() {
                            if let Some(parent_id_some) = parent_id {
                                if parent_id_some == c_parent.id {
                                    parent_id = Some(c_parent.id);
                                    true
                                } else {
                                    false
                                }
                            } else {
                                false
                            }
                        } else {
                            false
                        };
                    })
                    .map(|x| x.clone())
                    .collect();
            }
        }

        let filtered_names: Vec<Option<String>> = filtered.iter().map(|x| x.name.clone()).collect();
        dbg!(&filtered_names);

        filtered.first().map(|x| x.clone())
    }

    pub async fn get_user(&mut self, username: &str) -> Option<murmur_rpc::User> {
        let mut client = self.client.clone().unwrap();
        let user = client
            .user_get(tonic::Request::new(User {
                server: Some(murmur_rpc::Server::new()),
                name: Some(username.to_owned()),
                mute: None,
                deaf: None,
                suppress: None,
                priority_speaker: None,
                self_mute: None,
                self_deaf: None,
                recording: None,
                channel: None,
                online_secs: None,
                idle_secs: None,
                bytes_per_sec: None,
                version: None,
                plugin_context: None,
                plugin_identity: None,
                comment: None,
                texture: None,
                address: None,
                tcp_only: None,
                udp_ping_msecs: None,
                session: None,

                id: None,
                tcp_ping_msecs: None,
            }))
            .await
            .map(|x| x.into_inner())
            .ok();
        user
    }

    pub async fn move_user(&mut self, user: murmur_rpc::User, channel: murmur_rpc::Channel) {
        let mut client = self.client.clone().unwrap();

        let mut changed_user = user.clone();
        changed_user.channel = Some(channel.clone());
        client.user_update(tonic::Request::new(changed_user)).await;
    }

    pub async fn remove_channel(&mut self, channel: murmur_rpc::Channel) {
        let mut client = self.client.clone().unwrap();
        client.channel_remove(tonic::Request::new(channel)).await;
    }

    pub async fn kick_user(&mut self, user: murmur_rpc::User) {
        let mut client = self.client.clone().unwrap();
        client
            .user_kick(tonic::Request::new(murmur_rpc::user::Kick {
                server: user.clone().server,
                actor: None,
                reason: Some("Server shutdown".to_owned()),
                user: Some(user),
            }))
            .await;
    }
}

impl murmur_rpc::Server {
    pub fn new() -> Self {
        return murmur_rpc::Server {
            id: 1,
            uptime: None,
            running: None,
        };
    }
}
