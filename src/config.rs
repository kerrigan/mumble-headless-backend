use serde::Deserialize;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;

#[derive(Deserialize, Clone)]
pub struct Config {
    pub grpc_url: String,
    pub mumble_webproxy_host: String,
    pub mumble_webproxy_port: i32,
    pub auth_token: String,
    pub root_channel: String,

    pub listen_host: String,
    pub listen_port: i32,
}

pub fn parse_config(path: &str) -> Result<Config, Box<dyn Error>> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    let config = serde_json::from_reader(reader)?; //.map_err(|| "failed to read config")?;
    Ok(config)
}
