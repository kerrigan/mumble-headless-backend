use std::convert::Infallible;
use std::sync::Arc;

use hyper::{Body, Request, Server};
use routerify::prelude::*;
use routerify::{Middleware, Router, RouterService};
use tokio::sync::Mutex;

use murmurgrpc::MurmurGrpc;

use crate::config::parse_config;
use crate::murmurlistener::MurmurListener;
use crate::state::BroadcastCallState;

mod config;
mod handlers;
mod murmur;
mod murmur_rpc;
mod murmurgrpc;
mod murmurlistener;
mod state;
mod userinfo;

#[tokio::main]
async fn main() {
    let args: Vec<String> = std::env::args().collect();
    let config_path = args.get(1).unwrap_or(&"config.json".to_owned()).clone();
    let config = match parse_config(&config_path) {
        Ok(config) => config,
        Err(_) => {
            println!("Failed to load config");
            return;
        }
    };

    let listen_host = config.listen_host.clone();
    let listen_port = config.listen_port.clone();
    let root_channel = config.root_channel.clone();

    let mut rpc = MurmurGrpc::new(&config.clone().grpc_url, &config.clone().auth_token);

    let state = Arc::new(Mutex::new(BroadcastCallState::new(
        &config.clone().mumble_webproxy_host,
        config.clone().mumble_webproxy_port,
    )));

    let storage = state.clone();

    rpc.connect().await.unwrap();

    let router = Router::builder()
        .data(config.clone())
        .data(state.clone())
        .post("/api/new", handlers::new_call)
        .middleware(Middleware::pre(logger))
        .build()
        .unwrap();

    let (mut client2, mut receiver) =
        MurmurListener::new(&config.grpc_url.clone(), &config.auth_token.clone());
    client2.connect().await.unwrap();

    tokio::spawn(async move {
        let mut c2 = client2.clone();

        loop {
            let result = c2.listen_events().await;
            if let Ok(_) = result {
                break;
            }
        }
    });

    tokio::spawn(async move {
        loop {
            if let Some(msg) = receiver.recv().await {
                if msg.connected {
                    println!("User connected");
                    let mut storage = storage.lock().await;
                    if let Some(userinfo) = storage.get_user_info(&msg.username) {
                        let mut rpc = MurmurGrpc::new(&config.grpc_url, &config.auth_token);
                        rpc.connect().await.unwrap();
                        //TODO: mute user
                        let root_channel = config.root_channel.clone();

                        let user = rpc.get_user(&userinfo.username).await.unwrap();

                        let parent_channel = rpc.get_channel(&root_channel).await.unwrap();

                        //let channel = rpc.get_channel(userinfo.channelname.as_ref()).await;
                        let channel = rpc
                            .create_channel(parent_channel, userinfo.channelname.as_ref())
                            .await;
                        if let Ok(channel) = channel {
                            rpc.move_user(user, channel).await;
                        } else {
                            dbg!("Failed to create channel");
                        }
                        storage.user_joined(msg.username.clone());
                    }
                } else {
                    println!("User disconnected");
                    let mut rpc = MurmurGrpc::new(&config.grpc_url, &config.auth_token);
                    rpc.connect().await.unwrap();

                    let mut storage = storage.lock().await;

                    let root_channel = config.root_channel.clone();

                    let user_info = storage.get_user_info(&msg.username.clone());
                    if let Some(user_info) = user_info {
                        let full_channel_name =
                            format!("{}{}", root_channel.clone(), user_info.channelname.clone());

                        let channel_to_delete =
                            rpc.get_channel(&full_channel_name).await.map(|x| x.clone());
                        if channel_to_delete.is_some() {
                            rpc.remove_channel(channel_to_delete.unwrap()).await;
                        }
                    }

                    storage.user_leaved(msg.username.clone());
                }

                //dbg!(msg);
            } else {
                break;
            }
        }
    });

    let service = RouterService::new(router).unwrap();

    let addr = format!("{}:{}", listen_host, listen_port).parse().unwrap();
    let server = Server::bind(&addr).serve(service);

    println!("App is running on: {}", addr);

    let graceful = server.with_graceful_shutdown(graceful_shutdown(&root_channel, &mut rpc, state));

    if let Err(e) = graceful.await {
        eprintln!("server error: {}", e);
    }
}

async fn graceful_shutdown(
    root_channel: &str,
    rpc: &mut MurmurGrpc,
    state: Arc<Mutex<BroadcastCallState>>,
) {
    tokio::signal::ctrl_c()
        .await
        .expect("failed to install CTRL+C signal handler");

    println!("Shutdown server...");

    let state = state.lock().await;

    // Kick users
    let users = state.get_users();
    for user in users.iter() {
        let user = rpc.get_user(user).await;
        if let Some(user) = user {
            rpc.kick_user(user).await
        }
    }

    // Remove channels
    let channels = state.get_channels();
    for channel in channels.iter() {
        let channel = rpc
            .get_channel(&format!("{}{}", root_channel.clone(), channel))
            .await;

        if let Some(channel) = channel {
            rpc.clone().remove_channel(channel).await;
        }
    }
}

#[derive(Debug)]
struct State {
    channel: tonic::transport::Channel,
}

// A middleware which logs an http request.
async fn logger(req: Request<Body>) -> Result<Request<Body>, Infallible> {
    println!(
        "{} {} {}",
        req.remote_addr(),
        req.method(),
        req.uri().path()
    );
    Ok(req)
}
