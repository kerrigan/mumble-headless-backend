use std::convert::TryFrom;

use crate::userinfo::UserInfo;
use crate::{murmur, murmur_rpc};
use tokio::sync::mpsc;
use tokio::sync::mpsc::{UnboundedReceiver, UnboundedSender};
use tonic::transport::Channel;

#[derive(Clone)]
pub struct MurmurListener {
    client: Option<murmur_rpc::v1_client::V1Client<Channel>>,
    grpc_url: String,
    auth_token: String,
    sender: UnboundedSender<UserInfo>,
}

impl MurmurListener {
    pub fn new(grpc_url: &str, auth_token: &str) -> (Self, UnboundedReceiver<UserInfo>) {
        let (sender, receiver) = mpsc::unbounded_channel();
        (
            MurmurListener {
                client: None,
                grpc_url: grpc_url.to_owned(),
                auth_token: auth_token.to_owned(),
                sender,
            },
            receiver,
        )
    }

    pub async fn connect(&mut self) -> Result<bool, &'static str> {
        let client = murmur::create_sync_client(&self.grpc_url, &self.auth_token)
            .await
            .map_err(|_| "Failed connect async client")?;

        self.client = Some(client);

        Ok(true)
    }

    pub async fn listen_events(&mut self) -> Result<(), String> {
        let mut client = self.client.clone().unwrap();

        let mut stream = client
            .server_events(tonic::Request::new(murmur_rpc::Server::new()))
            .await
            .map_err(|err| format!("Failed to listen {:?}", err))?
            .into_inner();

        while let Some(message) = stream.message().await.map_err(|err| format!("{:?}", err))? {
            if let Some(msg_type) = message.r#type {
                let msg_type = murmur_rpc::server::event::Type::try_from(msg_type);
                if let Ok(msg_type) = msg_type {
                    match msg_type {
                        murmur_rpc::server::event::Type::UserConnected => {
                            if let Some(user) = message.user {
                                if let Some(username) = user.name {
                                    //user_connected(&username);
                                    let result = self.sender.send(UserInfo {
                                        connected: true,
                                        username,
                                    });
                                    //.await;
                                    if let Err(err) = result {
                                        dbg!("Connected send error {}", err.to_string());
                                    }
                                }
                            }
                            //println!("User connected")
                        }
                        murmur_rpc::server::event::Type::UserDisconnected => {
                            if let Some(user) = message.user {
                                if let Some(username) = user.name {
                                    let result = self.sender.send(UserInfo {
                                        connected: false,
                                        username,
                                    });
                                    //.await;
                                    if let Err(err) = result {
                                        dbg!("Disconnected send error {}", err.to_string());
                                    }
                                }
                            }
                            //println!("User disconnected")
                        }
                        murmur_rpc::server::event::Type::ChannelStateChanged => {
                            //dbg!(message);
                        }
                        _ => {
                            //dbg!(message);
                        }
                    }
                }
            } else {
                dbg!("Stream error");
            }
            //dbg!(message);
        }

        Ok(())
    }
}
