#[derive(Clone, Debug)]
pub struct UserInfo {
    pub connected: bool,
    pub username: String,
}
