use std::convert::Infallible;
use std::sync::Arc;

use crate::state::BroadcastCallState;
use tokio::sync::Mutex;
use url::form_urlencoded;

use crate::config::Config;
use hyper::{Body, Request, Response, StatusCode};
use routerify::prelude::*;
use std::collections::HashMap;

pub(crate) async fn new_call(mut req: Request<Body>) -> Result<Response<Body>, Infallible> {
    let body = req.body_mut();
    let body = hyper::body::to_bytes(body).await.unwrap();

    let params = form_urlencoded::parse(body.as_ref())
        .into_owned()
        .collect::<HashMap<String, String>>();

    dbg!(&params);

    //let params: HashMap<String, String> = HashMap::new();

    let user_name_opt = params.get("username");

    if user_name_opt.is_none() {
        let response = Response::builder()
            .status(StatusCode::UNAUTHORIZED)
            .body(Body::from("No username found"))
            .unwrap();
        return Ok(response);
    }

    let user_name = user_name_opt.unwrap();

    let mut state = req
        .data::<Arc<Mutex<BroadcastCallState>>>()
        .unwrap()
        .lock()
        .await;

    let config = req.data::<Config>().unwrap();

    let call_info = state.create_call(user_name);

    match call_info {
        Ok(call_info) => {
            let body = serde_json::to_vec(&call_info.to_front(&config.root_channel)).unwrap();
            Ok(Response::new(Body::from(body)))
        }
        Err(_) => {
            let response = Response::builder()
                .status(StatusCode::TOO_MANY_REQUESTS)
                .body(Body::from("Server is full"))
                .unwrap();
            Ok(response)
        }
    }
}
