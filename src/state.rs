use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use serde::Serialize;

use chrono::{DateTime, Duration, Utc};
use std::collections::HashMap;

pub struct BroadcastCallState {
    users: HashMap<String, CallInfo>,
    max_simultaneous_calls: usize,

    mumble_proxy_host: String,
    mumble_proxy_port: i32,
}

impl BroadcastCallState {
    pub fn new(mumble_proxy_host: &str, mumble_proxy_port: i32) -> Self {
        BroadcastCallState {
            users: HashMap::new(),
            max_simultaneous_calls: 10,

            mumble_proxy_host: mumble_proxy_host.to_owned(),
            mumble_proxy_port,
        }
    }
}

#[derive(Debug, Serialize, Clone)]
pub struct CallInfo {
    pub server: String,
    pub port: i32,
    pub username: String,
    pub channelname: String,

    #[serde(skip_serializing)]
    pub created: DateTime<Utc>,

    #[serde(skip_serializing)]
    pub connected: bool,
}

impl CallInfo {
    pub(crate) fn to_front(&self, root_channel: &str) -> Self {
        Self {
            server: self.server.clone(),
            port: self.port.clone(),
            username: self.username.clone(),
            channelname: root_channel.to_owned(),
            created: self.created,
            connected: self.connected,
        }
    }
}

impl BroadcastCallState {
    pub fn create_call(&mut self, username: &str) -> Result<CallInfo, String> {
        let users_count = self.users.len();
        let max_simultaneous_calls = self.max_simultaneous_calls;

        if users_count >= max_simultaneous_calls {
            // Clear dead calls
            let most_dead_user = self
                .users
                .iter()
                .filter(|&(username, info)| !info.connected)
                .filter(|&(username, info)| info.created < Utc::now() - Duration::minutes(1))
                .min_by_key(|a| a.1.created)
                .map(|(username, _)| username.clone());
            if let Some(most_dead_user) = most_dead_user {
                self.users.remove(&most_dead_user);
            } else {
                return Err("Server is full".to_owned());
            }
        }

        //TODO: server:port, username, channel

        let username_suffix: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(5)
            .map(char::from)
            .collect();

        let channelname: String = thread_rng()
            .sample_iter(&Alphanumeric)
            .take(8)
            .map(char::from)
            .collect();

        let created = Utc::now();

        let final_username = format!("{}_[{}]", username.to_owned(), username_suffix);

        let new_callinfo = CallInfo {
            server: self.mumble_proxy_host.clone(),
            port: self.mumble_proxy_port,
            username: final_username.clone(),
            channelname,
            created,
            connected: false,
        };

        self.users.insert(final_username, new_callinfo.clone());
        Ok(new_callinfo.clone())
    }

    pub fn user_joined(&mut self, username: String) {
        //TODO: move user to channel
        if let Some(user_info) = self.users.get(&username) {
            let mut new_user_info = user_info.clone();
            new_user_info.connected = true;
            self.users.insert(username, new_user_info);
        }
    }

    pub fn user_leaved(&mut self, username: String) {
        //TODO: remove info
        self.users.remove(&username);
    }

    pub fn get_user_info(&self, username: &str) -> Option<CallInfo> {
        return self.users.get(username).map(|x| x.clone());
    }

    pub fn get_users(&self) -> Vec<String> {
        self.users
            .iter()
            .map(|(username, _)| username.clone())
            .collect()
    }

    pub fn get_channels(&self) -> Vec<String> {
        self.users
            .iter()
            .map(|(_, info)| info.channelname.clone())
            .collect()
    }
}
