use tonic::metadata::MetadataValue;
use tonic::transport::{Channel, ClientTlsConfig};
use tonic::Request;

use std::convert::TryFrom;

use crate::murmur_rpc;

impl TryFrom<i32> for murmur_rpc::server::event::Type {
    type Error = &'static str;

    fn try_from(x: i32) -> Result<Self, Self::Error> {
        match x {
            0 => Ok(murmur_rpc::server::event::Type::UserConnected),
            1 => Ok(murmur_rpc::server::event::Type::UserDisconnected),
            2 => Ok(murmur_rpc::server::event::Type::UserStateChanged),
            3 => Ok(murmur_rpc::server::event::Type::UserTextMessage),
            4 => Ok(murmur_rpc::server::event::Type::ChannelCreated),
            5 => Ok(murmur_rpc::server::event::Type::ChannelRemoved),
            6 => Ok(murmur_rpc::server::event::Type::ChannelStateChanged),
            _ => Err("Can't cast"),
        }
    }
}

pub async fn create_sync_client(
    grpc_url: &str,
    auth_token: &str,
) -> Result<murmur_rpc::v1_client::V1Client<Channel>, String> {
    //let addr = "https://mmrpc.jugregator.org:443";
    //let addr = grpc_url.to_owned();
    let grpc_url = grpc_url.to_owned();
    let auth_token = auth_token.to_owned();

    let channel = Channel::from_shared(grpc_url)
        .unwrap()
        .tls_config(ClientTlsConfig::new())
        .map_err(|e| "sync client tls config error".to_owned())?
        .connect()
        .await
        .map_err(|e| "sync client connection error".to_owned())?;

    let auth_interceptor = move |mut req: Request<()>| {
        //grpc_auth != "Auth: foh7phoC"
        req.metadata_mut().insert(
            "authorization",
            MetadataValue::from_str(&format!("Auth {}", auth_token)).unwrap(),
        );
        Ok(req)
    };

    let client = murmur_rpc::v1_client::V1Client::with_interceptor(channel, auth_interceptor);

    Ok(client)
}
