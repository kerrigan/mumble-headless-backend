fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .out_dir("src")
        .build_server(false)
        //.type_attribute("Channel", "#[derive(Sync)]")
        .compile(&["proto/MurmurRPC.proto"], &["proto"])
        .unwrap();
    Ok(())
}
